<?php
//Variaveis para armazenar os dados preenchidos no formulário de alteração de usuario.
$nome = filter_input(INPUT_POST, "nome");
$email = filter_input(INPUT_POST, "email");
$senha = trim(filter_input(INPUT_POST, "senha"));

//Verifica se o campo de senha foi preenchido, se sim então encriptografa a nova senha.
if($senha != "") {
	$senha = crypt("salt", $senha);
}

//Inicia a sessão buscando o dado de ID do usuario.
session_start();
$id_usuario = $_SESSION['ID_Usuario'];

//Conexão com o banco de dados.
$link = mysqli_connect("localhost", "movieguidetmd", "M0V13", "movieguide");

//Se a conexão estiver funcionando então altera os valores de nome e e-mail do usuario no banco.
if($link) {
	$query = "update tb_usuario set Nome_Usuario='$nome', Email_Usuario='$email' ";

//Se a variavel de senha não estiver vazia então altera o dado no banco.	
	if($senha != "") {
		$query .= ", Senha_Usuario='$senha'";
	}

//Altera esses dados onde o ID do usuario no banco seja igual ao ID da sessão aberta (logado no site).	
	$query .= " where ID_Usuario='$id_usuario';";
	
	$resultado = mysqli_query($link, $query);

//Redireciona para a página de configurações após a conclusão do processo	
	if($resultado){
		echo ("<script> alert('Dados Alterados.');</script>");
		echo "<script> window.location='configuracoes.php'; </script>";
//		header ("Location: configuracoes.php");
	} else {
		die("Erro: ".mysqli_error($link));
	} 
	//else {
//	die("Erro: ". mysqli_error($link));

	}


?>
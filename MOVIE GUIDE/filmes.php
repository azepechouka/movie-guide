<?php
//Conexão com o banco de dados.
$host = "localhost";
$user = "movieguidetmd";
$pass = "M0V13";
$banco = "movieguide";
$conexao = mysql_connect($host, $user, $pass) or die (mysql_error ());
mysql_select_db($banco) or die (mysql_error());

$result=mysql_query("SELECT * FROM tb_filme  order by Lancamento_Filme") or die("Impossível executar a query"); 

$id = filter_input(INPUT_GET, "ID_Filme");
$titulo = filter_input(INPUT_GET, "Titulo_Filme");
$diretor = filter_input(INPUT_GET, "Diretor_Filme");
$elenco = filter_input(INPUT_GET, "Elenco_Filme");
$sinopse = filter_input(INPUT_GET, "Sinopse_Filme");
$duracao = filter_input(INPUT_GET, "Duracao_Filme");
$generos = filter_input(INPUT_GET, "Generos_Filme");
$lancamento = filter_input(INPUT_GET, "Lancamento_Filme");
$nacionalidade = filter_input(INPUT_GET, "Nacionalidades_Filme");

//Verifica se já possui uma sessão aberta, se não redireciona para a index.
	session_start ();
	if(!isset($_SESSION['email']) || !isset($_SESSION['senha'])) {
		header("Location: index.php");
		exit;
}
?>
<!DOCTYPE html>
<html lang = "Pt-br">
<head>
	<meta charset="content-Type: text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="estilo.css"/>
	<title> MOVIE GUIDE </title>
</head>
<body link="#D0D0D0" alink="#D0D0D0" vlink="#D0D0D0">
	<div id="mestre">
	<!-- Div utilizada para o menu lateral do site. -->
		<div id="teste">
		</div>
		<div id="lateral">
			</br>
			<!-- Imagem da logo do site. -->
			<img class="center" src="IMAGENS\claquete.png" height="20%" width="80%" />
			</br>
			<!-- Div utilizada para a criação da caixa de busca. -->
			<div id="busca">
				<input type="text" id="txtBusca" placeholder="Buscar..."/>
				<img src="IMAGENS\search.png" height="21px" id="btnBusca" alt="Buscar"/>
			</div>
			
			<!-- Lista não ordenada, utilizada para a criação do menu e estilizada no CSS. -->
			<ul class="center">
				<li> <a href="filmes.php"> Filmes </a> </li> </br>
				<li> <a href="embreve.php"> Em Breve </a> </li> </br>
				<li> <a href="assistir.php"> Para Assistir </a> </li> </br>
				<li> <a href="assistidos.php"> Assistidos </a> </li> 
			</ul>
			<hr width="80%" align="center">
			<ul class="center">
				<li> <?=$_SESSION['Nome_Usuario'];?> </li> </br>
				<li> <a href="contato.php"> Contato </a> </li> </br>
				<li> <a href="configuracoes.php"> Configurações </a> </li>
			</ul>
			<hr width="80%" align="center">
			<ul class="center">
				<li align="center"><a href="logout.php"> SAIR </a></li>
			</ul>
			<hr width="80%" align="center">
			<!-- Utilizada a tabela para a colocação das imagens dos icones das redes sociais lado a lado. -->
			<table class="center" width="80%" height="50px">
				<tr>
					<td width="35%" > <img class="social" src="IMAGENS\facebook.png" alt="Facebook"/> </td>
					<td width="35%" > <img class="social" src="IMAGENS\instagram.png" alt="Instagram"/> </td>
					<td width="35%" > <img class="social" src="IMAGENS\twitter.png" alt="Twitter"/> </td>
				</tr>
			</table>
			<hr width="80%" align="center">
			<ul class="center">
				<li align="center">
				<?php
				if($_SESSION['ID_Tipo_Usuario'] =='1') {
				?>	<a href="adm.php"> Administrador </a>
				<?php
				}
				?>
				</li>
			</ul>
		</div>
		<!-- Div utilizada para área onde irá ficar todo o conteudo do site. No caso os posters dos filmes. -->
		<div id="direita"> 
			<!-- Div utilizada para colocar a escrita que irá aparecer no topo de cada página. -->
			<div id="topo">
				<p> FILMES </p>
			</div>
			<!-- Foi criada uma div para cada poster de filme colocado no site. Foi utilizada uma classe para configurar através do CSS o tamanho da imagem e da margem, para que ficassem todos os posters com aparência padronizada. -->
			<?php 
			while($row=mysql_fetch_object($result)) { ?>
			<div class="poster">
				<a href="#openModal_<?=$row->ID_Filme?>" >
				<?php
				echo "<img class='grow' width='100%' src='getImagem.php?PicNum=$row->ID_Filme' \">"; 
				 ?> </a>
			</div>
			<div id="openModal_<?=$row->ID_Filme?>" class="modalDialog" style="overflow-y: scroll;">
				<div>
					<a href="#close" title="Close" class="close">X</a>
					<h6><?php echo $row->Titulo_Filme ?></h6>
					<?php echo "<img class='filmes' src='getImagem.php?PicNum=$row->ID_Filme' \">";?>
					<h1>Data de lançamento:</br><?php echo $row->Lancamento_Filme ?> </h1>
					<h1>Duração: <?php echo $row->Duracao_Filme ?></h1>
					<h1>Direção: <?php echo $row->Diretor_Filme ?></h1>
					<h1>Elenco: <?php echo $row->Elenco_Filme ?> </h1>
					<h1>Gêneros: <?php echo $row->Generos_Filme ?> </h1>
					<h1>Nacionalidade: <?php echo $row->Nacionalidades_Filme ?>  </h1>
					<h1>Sinopse:</br>
					<?php echo $row->Sinopse_Filme ?> </h1>	
					<table class="center" width="100%" height="50px">
						<tr>
							<td width="16%" > <a href="categoria.php?favorito=1&id_filme=<?=$row->ID_Filme?>"> <img class="social" src="IMAGENS\heartout.png" alt="Favorito"/> </a> </td>
							<td width="16%"> <h1> Favorito </h1> </td>
							<td width="16%" > <a href="categoria.php?assistir=1&id_filme=<?=$row->ID_Filme?>" > <img class="social" src="IMAGENS\filmout.png" alt="Assistir"/> </a> </td>
							<td width="16%"> <h1> Quero Assistir </h1> </td>
							<td width="16%" > <a href="categoria.php?assistido=1&id_filme=<?=$row->ID_Filme?>"> <img class="social" src="IMAGENS\checkout.png" alt="Assistido"/> </a> </td>
							<td> <h1> Já Assisti </h1> </td>
						</tr>
					</table>	
				</div>
			</div>
			<?php }  ?>
			
			
		</div> 
	</div>
</body>
</html>
﻿<?php
//Conexão com o banco de dados.
$host = "localhost";
$user = "movieguidetmd";
$pass = "M0V13";
$banco = "movieguide";
$conexao = mysql_connect($host, $user, $pass) or die (mysql_error ());
mysql_select_db($banco) or die (mysql_error());

//Verifica se já possui uma sessão aberta, se não redireciona para a index.
	session_start ();
	if(!isset($_SESSION['email']) || !isset($_SESSION['senha'])) {
		header("Location: index.php");
		exit;
}
?>
<!DOCTYPE html>
<html lang = "Pt-br">
<head>
	<meta charset="content-Type: text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="estilo.css"/>
	<title> MOVIE GUIDE </title>
</head>
<body link="#D0D0D0" alink="#D0D0D0" vlink="#D0D0D0">
	<div id="mestre">
	<!-- Div utilizada para o menu lateral do site. -->
		<div id="teste">
		</div>
		<div id="lateral">
			</br>
			<!-- Imagem da logo do site. -->
			<img class="center" src="IMAGENS\claquete.png" height="20%" width="80%" />
			</br>
			<!-- Div utilizada para a criação da caixa de busca. -->
			<div id="busca">
				<input type="text" id="txtBusca" placeholder="Buscar..."/>
				<img src="IMAGENS\search.png" height="21px" id="btnBusca" alt="Buscar"/>
			</div>
			
			<!-- Lista não ordenada, utilizada para a criação do menu e estilizada no CSS. -->
			<ul class="center">
				<li> <a href="filmes.php"> Filmes </a> </li> </br>
				<li> <a href="embreve.php"> Em Breve </a> </li> </br>
				<li> <a href="assistir.php"> Para Assistir </a> </li> </br>
				<li> <a href="assistidos.php"> Assistidos </a> </li> 
			</ul>
			<hr width="80%" align="center">
			<ul class="center">
				<li> <?=$_SESSION['Nome_Usuario'];?> </li> </br>
				<li> <a href="contato.php"> Contato </a> </li> </br>
				<li> <a href="configuracoes.php"> Configurações </a> </li>
			</ul>
			<hr width="80%" align="center">
			<ul class="center">
				<li align="center"><a href="logout.php"> SAIR </a></li>
			</ul>
			<hr width="80%" align="center">
			<!-- Utilizada a tabela para a colocação das imagens dos icones das redes sociais lado a lado. -->
			<table class="center" width="80%" height="50px">
				<tr>
					<td width="35%" > <img class="social" src="IMAGENS\facebook.png" alt="Facebook"/> </td>
					<td width="35%" > <img class="social" src="IMAGENS\instagram.png" alt="Instagram"/> </td>
					<td width="35%" > <img class="social" src="IMAGENS\twitter.png" alt="Twitter"/> </td>
				</tr>
			</table>
			<hr width="80%" align="center">
			<ul class="center">
				<li align="center">
				<?php
				if($_SESSION['ID_Tipo_Usuario'] =='1') {
				?>	<a href="adm.php"> Administrador </a>
				<?php
				}
				?>
				</li>
			</ul>
		</div>
		<!-- Div utilizada para área onde irá ficar todo o conteudo do site. No caso a mensagem de que a mensagem ou sugestão do formulario de contato foi enviada. -->
		<div id="direitacont"> 
			<center>	
			<p> SUA SUGESTÃO/MENSAGEM FOI ENVIADO COM SUCESSO. </p>
			<img src="IMAGENS\logod.png" width="50%"/>
			</center>
		</div> 
	</div>
</body>
</html>